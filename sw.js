const CACHE_NAME = "castellanos-v1"

const cache_urls = ["/offline/view.html",
                   "/offline/style.css",
                   "/offline/map.png"]

self.addEventListener("install", function(ev){
  console.log("SW instalada")

  caches.open(CACHE_NAME)
        .then(function(cache){
          console.log("cached opened")
          return cache.addAll(cache_urls)
        })

})

self.addEventListener("fetch", function(ev){
  console.log(ev.request)
  ev.respondWith(
    caches.match(ev.request).then(function(response){
            if (response) {
              return response
            }else{
              return fetch(ev.request)
            }
          }).catch(function(err){
            if(ev.request.mode == "navigate"){
              return caches.match("offline/view.html")
            }
          })



  )
})