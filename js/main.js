if (navigator.serviceWorker) {
  navigator.serviceWorker.register("./sw.js")
}

(function() {

  let pinged = false;
  let nav = document.querySelector(".navigation");
  let stickyScrollPoint = document.querySelector(".hero-image").offsetHeight;

  const email = "ajpalacios.sist@gmail.com";

  isOpen()

  $("#contact-form").on("submit",function(ev){
    ev.preventDefault()

    sendForm($(this))

    return false
  })

  function pingToTop() {
    if (pinged) return;
    nav.classList.add("pined");
    pinged = true;
  }

  function unPingFromTop() {
    if (!pinged) return;
    nav.classList.remove("pined");
    pinged = false;
  }

  window.addEventListener('scroll', function(ev) {
    let coords = nav.getBoundingClientRect();
    console.log(coords)
    if (window.scrollY <= stickyScrollPoint) return unPingFromTop();
    if (coords.top < 0) pingToTop();
    console.log(stickyScrollPoint)

  });

  // navigation scroll
  $("nav a").click(function(event){
    var id = $(this).attr("href");
    let navh = document.querySelector(".navigation li");
    let offset = navh.getBoundingClientRect()
    var target=$(id).offset().top - offset.height;

    $("html, body").animate(
      {
        scrollTop: target
      },
      500
    );
    event.preventDefault();
  });

  $("#menu-opener").on('click', toggleNav);

  $(".menu-link").on('click', toggleNav);


  function toggleNav(){
    $("#responsive-nav ul").toggleClass("active");
    $(".menu").toggleClass("fa-bars");
  }

  function sendForm($form){

    console.log($form.formObject())

      $.ajax({
        url: $form.attr("action"),
        method: "POST",
        data: $form.formObject(),
        dataType: "json",
        success: function(){
          alert("Todo salio bien")
        }
    });
  }

  function isOpen(){

    let date = new Date()
    const current_hour = date.getHours()

    if (current_hour < 17 || current_hour > 21) {
      console.log("Cerrado")

      $("#isOpen").html("Cerrado Ahora <br><br> Abierto de Lunes a Viernes de 5:00pm a 9:00pm <br> y Sábado de 11:00am a 1:00pm <br><br>Telefonos:<br><br>55-51-96-18-98<br> y<br><br>55-34-60-09-96")

    }

  }

  $('.special.cards .image').dimmer({
    on: 'hover'
  });

})();
